php-symfony-polyfill (1.29.0-3) unstable; urgency=medium

  * Force system dependencies loading

 -- David Prévot <taffit@debian.org>  Tue, 05 Mar 2024 17:27:42 +0100

php-symfony-polyfill (1.29.0-2) unstable; urgency=medium

  * Drop some tests failing on 32-bit architectures

 -- David Prévot <taffit@debian.org>  Wed, 07 Feb 2024 08:40:17 +0100

php-symfony-polyfill (1.29.0-1) unstable; urgency=medium

  [ Tim Düsterhus ]
  * Fix the `ini_parse_quantity()` polyfill

  [ Nicolas Grekas ]
  * Update CHANGELOG

  [ Ayesh Karunaratne ]
  * [PHP 8.3] Add `SQLite3Exception` class
  * [PHP 8.4] Add initial stubs

  [ Fan2Shrek ]
  * Add str_increment and str_decrement functions

  [ David Prévot ]
  * Drop incompatible option with PHPUnit 10
  * Drop versioned build-dependencies
  * Update copyright (years)
  * New php-symfony-polyfill-php84 package

 -- David Prévot <taffit@debian.org>  Wed, 31 Jan 2024 00:07:43 +0100

php-symfony-polyfill (1.28.0-1) unstable; urgency=medium

  [ Ion Bazan ]
  * Use static data providers
  * [PHP 8.3] Add `#[\Override]` attribute
  * [PHP 8.3] Polyfill mb_str_pad()
  * [PHP 8.3] Polyfill new non-overloaded variants of functions

  [ Ayesh Karunaratne ]
  * [PHP 8.1] Add `CURLStringFile` polyfill
  * [PHP 8.3] Add Exception/Error classes for `Date/Time` extension

  [ Nicolas Grekas ]
  * Update CHANGELOG

  [ Jérémy Romey ]
  * add mb_check_encoding with array value

  [ Michael Babker ]
  * Update return type for `IntlDateFormatter::format()`

  [ Alexander M. Turek ]
  * Add odbc_connection_string_*() functions

  [ Alexandre Daubois ]
  * Add case folding map

  [ Greg Roach ]
  * Add polyfill for ini_parse_quantity based on the C code in PHP

  [ David Prévot ]
  * Update packages description

 -- David Prévot <taffit@debian.org>  Sun, 03 Sep 2023 02:38:57 +0530

php-symfony-polyfill (1.27.0-2) unstable; urgency=medium

  * Drop (data) tests failing with PHP 8.2 (Closes: #1028861)
  * Update standards version to 4.6.2, no changes needed.

 -- David Prévot <taffit@debian.org>  Sat, 14 Jan 2023 19:45:38 +0100

php-symfony-polyfill (1.27.0-1) unstable; urgency=medium

  * Source-only upload after NEW processing

 -- David Prévot <taffit@debian.org>  Wed, 07 Dec 2022 08:13:52 +0100

php-symfony-polyfill (1.27.0-1~new) unstable; urgency=medium

  [ Damien Alexandre ]
  * [Intl] Fix the IntlDateFormatter::formatObject signature

  [ Ion Bazan ]
  * Skip PHP_FLOAT_* checks on PHP 7.3+
  * Add json_validate polyfill

  [ Nicolas Grekas ]
  * Prepare for v1.27

  [ Tim Düsterhus ]
  * Polyfill the random extension's interfaces and Secure engine

  [ David Prévot ]
  * New php-symfony-polyfill-php83 package
  * Drop patch not needed anymore
  * Use php-mysql for tests
  * Workaround ICU new format (Closes: #1024320)

 -- David Prévot <taffit@debian.org>  Tue, 06 Dec 2022 15:32:39 +0100

php-symfony-polyfill (1.26.0-3) unstable; urgency=medium

  * Adapt precision for recent phpunit-comparator

 -- David Prévot <taffit@debian.org>  Thu, 22 Sep 2022 08:10:14 +0200

php-symfony-polyfill (1.26.0-2) unstable; urgency=medium

  * Build on buildd after NEW processing

 -- David Prévot <taffit@debian.org>  Fri, 17 Jun 2022 14:16:39 +0200

php-symfony-polyfill (1.26.0-1) unstable; urgency=medium

  [ Nicolas Grekas ]
  * Fix list of currencies
  * Fix changelog

  [ Athos Ribeiro ]
  * Fix QQQQQ date format

  [ Laurent Laville ]
  * update Php72 README about mbstring functions

  [ Simon Podlipsky ]
  * Passing null to preg_split() throws deprecation on PHP 8.1

  [ David Prévot ]
  * Add php-symfony-polyfill-php82

 -- David Prévot <taffit@debian.org>  Sat, 04 Jun 2022 10:52:27 +0200

php-symfony-polyfill (1.25.0-4) unstable; urgency=medium

  * Fix list of currencies
  * Mark two more packages as Multi-Arch: foreign

 -- David Prévot <taffit@debian.org>  Tue, 31 May 2022 08:24:49 +0200

php-symfony-polyfill (1.25.0-3) unstable; urgency=medium

  [ Athos Ribeiro ]
  * Add support to libicu70 (Closes: #1011786)

  [ David Prévot ]
  * Simpler fix for #1007236
  * Mark most packages as Multi-Arch: foreign
  * Update Standards-Version to 4.6.1

 -- David Prévot <taffit@debian.org>  Thu, 26 May 2022 22:41:38 +0200

php-symfony-polyfill (1.25.0-2) unstable; urgency=medium

  * Team upload.
  * d/patches: Revert upstream commit 1633f18 to avoid marking php-symfony-
    polyfill-$FOO as 'Provides: php-$FOO'.  Symfony\Polyfill\$FOO aren't
    drop-in replacements for PHP's ext-$FOO outside Symfony applications
    (Closes: #1007236)

 -- Guilhem Moulin <guilhem@debian.org>  Mon, 14 Mar 2022 20:20:04 +0100

php-symfony-polyfill (1.25.0-1) unstable; urgency=medium

  [ Nicolas Grekas ]
  * [Php80] Fix test for fdiv() (Closes: #1006734)

 -- David Prévot <taffit@debian.org>  Mon, 07 Mar 2022 00:32:13 +0100

php-symfony-polyfill (1.24.0-1) unstable; urgency=medium

  [ Nicolas Grekas ]
  * Update changelog

  [ Cédric Anne ]
  * Indicates that polyfills provides corresponding extensions

 -- David Prévot <taffit@debian.org>  Sat, 08 Jan 2022 15:07:34 -0400

php-symfony-polyfill (1.23.1-2) unstable; urgency=medium

  * Fix d/watch after hosting change
  * Drop patch now that Symfony 5 is available
  * Use recent php-symfony-intl for CI

 -- David Prévot <taffit@debian.org>  Wed, 24 Nov 2021 22:47:08 -0400

php-symfony-polyfill (1.23.1-1) unstable; urgency=medium

  * Upload to unstable now that Bullseye has been released

  [ Nicolas Grekas ]
  * Fixed `grapheme_str(r)ipos()`

  [ Alexander M. Turek ]
  * Allow null in str_*() functions

  [ David Prévot ]
  * Use dh-sequence-phpcomposer instead of pkg-php-tools
  * Generate phpabtpl at build time
  * Update standards version to 4.6.0, no changes needed

 -- David Prévot <taffit@debian.org>  Thu, 07 Oct 2021 16:31:59 -0400

php-symfony-polyfill (1.23.0-1) experimental; urgency=medium

  * Upload to experimental during the freeze

  [ Nicolas Grekas ]
  * Remove INTL_IDNA_VARIANT_2003 on PHP 8
  * [PHP 8.1] add enum_exists()
  * Fix IntlDateFormatter::format() on 32b platforms

  [ Ion Bazan ]
  * [PHP 8.1] Add MYSQLI_REFRESH_REPLICA polyfill

  [ Dennis Wilke ]
  * [mbstring] add return value to mb_parse_str (#351)

  [ Alexander M. Turek ]
  * Add ReturnTypeWillChange attribute

  [ David Prévot ]
  * Update descriptions
  * Revert intl currencies data update

 -- David Prévot <taffit@debian.org>  Sun, 20 Jun 2021 23:30:19 -0400

php-symfony-polyfill (1.22.1-1) unstable; urgency=medium

  [ Nicolas Grekas ]
  * Always accept null values on PHP 8

  [ David Prévot ]
  * gbp.conf: Fix upstream-vcs-tag

 -- David Prévot <taffit@debian.org>  Tue, 16 Feb 2021 13:52:24 -0400

php-symfony-polyfill (1.22.0-3) unstable; urgency=medium

  * d/gbp.conf: Set upstream-vcs-tag
  * d/pkg-php-tools-overrides: drop empty lines

 -- David Prévot <taffit@debian.org>  Mon, 01 Feb 2021 20:07:30 -0400

php-symfony-polyfill (1.22.0-2) unstable; urgency=medium

  * Fix autopkgtest call

 -- David Prévot <taffit@debian.org>  Sat, 16 Jan 2021 18:07:01 -0400

php-symfony-polyfill (1.22.0-1) unstable; urgency=medium

  * Upload to unstable

 -- David Prévot <taffit@debian.org>  Thu, 14 Jan 2021 18:04:29 -0400

php-symfony-polyfill (1.22.0-1~exp1) experimental; urgency=medium

  * Upload to experimental to avoid interfering with phpunit migration

  [ Nicolas Grekas ]
  * Fix signatures of polyfilled classes for PHP >= 8
  * Add polyfill-php81

  [ David Prévot ]
  * Drop backward compatibility for PHPUnit
  * Update copyright (years)
  * Add new php-symfony-polyfill-php81 component
  * Install /u/s/p/autoloaders file
  * Don’t try and find class in bootstrap
  * Use php-symfony-var-dumper for tests (with PHP 8)

 -- David Prévot <taffit@debian.org>  Mon, 11 Jan 2021 11:50:33 -0400

php-symfony-polyfill (1.21.0-2) unstable; urgency=medium

  * Drop php-symfony-intl loading, only files are needed for tests

 -- David Prévot <taffit@debian.org>  Sun, 06 Dec 2020 07:43:26 -0400

php-symfony-polyfill (1.21.0-1) unstable; urgency=medium

  [ Nicolas Grekas ]
  * Bump minimum PHP version to 7.1
  * Drop polyfills for PHP <= 7.1 from the metapackage

  [ David Prévot ]
  * Rename main branch to debian/latest (DEP-14)
  * Update watch file format version to 4.
  * Update Standards-Version to 4.5.1
  * Adapt packaging to removed and updated components
  * Add Debian-specific patches to run tests

 -- David Prévot <taffit@debian.org>  Wed, 02 Dec 2020 09:34:22 -0400

php-symfony-polyfill (1.18.1-1) unstable; urgency=medium

  [ Trevor Rowbotham ]
  * Don't force labels containing URL delimiters to stay in their
    Unicode form when using idn_to_ascii

 -- David Prévot <taffit@debian.org>  Thu, 06 Aug 2020 16:20:35 +0200

php-symfony-polyfill (1.18.0-1) unstable; urgency=medium

  [ Ayesh Karunaratne ]
  * Add `UnhandledMatchError` exception class

  [ Trevor Rowbotham ]
  * Replace underlying IDN library

  [ David Prévot ]
  * Set Rules-Requires-Root: no.
  * Update copyright
  * Update php-symfony-polyfill-php80 description
  * Drop patch not needed anymore

 -- David Prévot <taffit@debian.org>  Wed, 15 Jul 2020 23:28:14 -0400

php-symfony-polyfill (1.17.1-1) unstable; urgency=medium

  [ Nicolas Grekas ]
  * Fix accuracy of Normalizer::isNormalized()

 -- David Prévot <taffit@debian.org>  Mon, 29 Jun 2020 06:06:09 -1000

php-symfony-polyfill (1.17.0-2) unstable; urgency=medium

  * Source only upload

 -- David Prévot <taffit@debian.org>  Thu, 18 Jun 2020 08:16:59 -1000

php-symfony-polyfill (1.17.0-1) unstable; urgency=medium

  [ Nicolas Grekas ]
  * Update CHANGELOG
  * Add polyfill for PHP8's Stringable

  [ Grégoire Pineau ]
  * [Uuid] Added the polyfill

  [ Ion Bazan ]
  * add new PHP 7.4 functions

  [ Nicolas Grekas ]
  * Add polyfill for Intl\MessageFormatter

  [ Laurent Bassin ]
  * [Intl] Add IDN polyfill structure

  [ Scott ]
  * Prevent DoS via long passwords [CVE-2013-5958]

  [ SAMUEL NELA ]
  * Update year in license file

  [ David Prévot ]
  * d/control:
    - Use debhelper-compat 13
    - Use Standards-Version 4.5.0
    - Update php-symfony-polyfill-73 description
    - Add new php-symfony-polyfill-intl-idn component
    - Add new php-symfony-polyfill-intl-messageformatter component
    - Add new php-symfony-polyfill-php74 component
    - Add new php-symfony-polyfill-uuid component
    - Add new php-symfony-polyfill-php80 component
    - Drop versioned dependency satisfied in (old)stable
    - Use php-uuid for testsuite
  * d/copyright:
    - Use https in debian/copyright
    - Update years
  * d/clean
    - Use it for directories
    - Extend for PHPUnit 8
  * d/rules:
    - Drop get-orig-source target
    - Simplify override_dh_auto_test
    - Use --sourcedirectory instead of --sourcedir
  * Update homemade autoload, and install it for the testsuite
  * d/upstream/metadata: Set upstream metadata fields,
    Bug-Database, Bug-Submit, Repository, Repository-Browse.
  * d/patches: Drop test currently failing (Closes: #911832)
  * d/pkg-php-tools-overrides: Override common pkg-php-tools-overrides
  * d/tests/control:
    - Use php-uuid for testsuite
    - Allow stderr for CI

 -- David Prévot <taffit@debian.org>  Sun, 31 May 2020 11:43:00 -1000

php-symfony-polyfill (1.9.0-1) unstable; urgency=medium

  [ Fabien Potencier ]
  * bumped version to 1.9

  [ David Prévot ]
  * Drop now useless autoload override for php73
  * Update Standards-Version to 4.2.0

 -- David Prévot <taffit@debian.org>  Thu, 09 Aug 2018 14:24:03 +0800

php-symfony-polyfill (1.8.0-1) unstable; urgency=medium

  [ David Prévot ]
  * Fix Vcs-* URLs
  * Update homemade autoload to new dependencies
  * Add new php-symfony-polyfill-ctype component
  * Add new php-symfony-polyfill-php73 component
  * Update Standards-Version to 4.1.5
  * Update for PHPUnit 7.2 (Closes: 895604)

  [ Gabriel Caruso ]
  * Add is_countable under PHP 7.3

  [ BackEndTea ]
  * Add the cytpe polyfill

 -- David Prévot <taffit@debian.org>  Thu, 05 Jul 2018 11:01:36 -1000

php-symfony-polyfill (1.7.0-1) unstable; urgency=medium

  [ Nicolas Grekas ]
  * Update LICENSE for 2018
  * Add compatibility with namespaced PHPUnit

  [ Robert Gust-Bardon ]
  * Add polyfills for mb_*_numericentity

  [ David Prévot ]
  * Revert "Move to namespaced phpunit versions"
  * Update copyright (years)
  * Move project repository to salsa.d.o
  * Update Standards-Version to 4.1.3

 -- David Prévot <taffit@debian.org>  Tue, 27 Feb 2018 22:20:59 -1000

php-symfony-polyfill (1.6.0-2) unstable; urgency=medium

  * Move to namespaced phpunit versions (Closes: 882930)

 -- David Prévot <taffit@debian.org>  Fri, 01 Dec 2017 17:30:05 -1000

php-symfony-polyfill (1.6.0-1) unstable; urgency=medium

  [ Nicolas Grekas ]
  * Add SessionUpdateTimestampHandlerInterface polyfill

  [ David Prévot ]
  * Update Standards-Version to 4.1.1

 -- David Prévot <taffit@debian.org>  Thu, 19 Oct 2017 16:43:59 -1000

php-symfony-polyfill (1.5.0-1) unstable; urgency=medium

  [ Nicolas Grekas ]
  * Polyfills for mb_chr(), mb_ord() and mb_scrub()
  * Allow Symfony 4.0
  * Add Php72 polyfill, abandon Xml polyfill

  [ Richard Fussenegger ]
  * Added PHP_INT_MIN constant

  [ David Prévot ]
  * Install main README in php-symfony-polyfill
  * Update copyright (years)
  * Update for new binary packages
    - php-symfony-polyfill-php71
    - php-symfony-polyfill-php72
  * Update Standards-Version to 4.1.0
  * Require bootstrap.php at a latter stage

 -- David Prévot <taffit@debian.org>  Fri, 22 Sep 2017 12:19:33 -1000

php-symfony-polyfill (1.2.0-1) unstable; urgency=medium

  [ Nicolas Grekas ]
  * Add CHANGELOG

  [ Andreas Lemke ]
  * allow paragonie/random_compat 2.0

  [ David Prévot ]
  * Drop now useless recommendation
  * Update Standards-Version to 3.9.8
  * Document that two tests are failing with php-apcu-bc

  [ Fabien Potencier ]
  * bumped version to 1.2

 -- David Prévot <taffit@debian.org>  Thu, 19 May 2016 20:50:35 -0400

php-symfony-polyfill (1.1.1-1) unstable; urgency=medium

  [ David Prévot ]
  * Fix composer.json syntax
  * Update php-symfony-polyfill-intl-icu description
  * Trim autoload.php for Apcu (removed Resources/stubs)
  * PHP 7.0 transition:
    - Depend on php-* instead of php5-*
    - Build with recent pkg-php-tools

 -- David Prévot <taffit@debian.org>  Sat, 12 Mar 2016 15:55:39 -0400

php-symfony-polyfill (1.1.0-1) unstable; urgency=low

  * Initial release (new symfony and owncloud dependencies)

 -- David Prévot <taffit@debian.org>  Sun, 14 Feb 2016 21:25:14 -0400
