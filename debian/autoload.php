<?php

require_once __DIR__ . '/Apcu/autoload.php';
require_once __DIR__ . '/Ctype/autoload.php';
require_once __DIR__ . '/Iconv/autoload.php';
require_once __DIR__ . '/Intl/Grapheme/autoload.php';
require_once __DIR__ . '/Intl/Icu/autoload.php';
require_once __DIR__ . '/Intl/Idn/autoload.php';
require_once __DIR__ . '/Intl/MessageFormatter/autoload.php';
require_once __DIR__ . '/Intl/Normalizer/autoload.php';
require_once __DIR__ . '/Mbstring/autoload.php';
// require_once __DIR__ . '/Php72/autoload.php'; (already required by Intl/Idn/autoload.php)
require_once __DIR__ . '/Php73/autoload.php';
require_once __DIR__ . '/Php74/autoload.php';
require_once __DIR__ . '/Php80/autoload.php';
require_once __DIR__ . '/Php81/autoload.php';
require_once __DIR__ . '/Php82/autoload.php';
require_once __DIR__ . '/Php83/autoload.php';
require_once __DIR__ . '/Php84/autoload.php';
require_once __DIR__ . '/Util/autoload.php';
require_once __DIR__ . '/Uuid/autoload.php';
require_once __DIR__ . '/Xml/autoload.php';

require_once __DIR__ . '/bootstrap.php';
