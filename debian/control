Source: php-symfony-polyfill
Section: php
Priority: optional
Maintainer: Debian PHP PEAR Maintainers <pkg-php-pear@lists.alioth.debian.org>
Uploaders: Daniel Beyer <dabe@deb.ymc.ch>, David Prévot <taffit@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-sequence-phpcomposer,
               jq,
               php-intl,
               php-ldap,
               php-mbstring,
               php-mysql,
               php-symfony-intl,
               php-symfony-var-dumper,
               php-uuid,
               phpab,
               phpunit
Build-Conflicts: php-apcu-bc
Standards-Version: 4.6.2
Homepage: https://symfony.com
Vcs-Git: https://salsa.debian.org/php-team/pear/php-symfony-polyfill.git -b debian/latest
Vcs-Browser: https://salsa.debian.org/php-team/pear/php-symfony-polyfill
Rules-Requires-Root: no

Package: php-symfony-polyfill
Architecture: all
Depends: ${misc:Depends},
         ${phpcomposer:Debian-replace},
         ${phpcomposer:Debian-require}
Suggests: ${phpcomposer:Debian-suggest}
Recommends: ${phpcomposer:Debian-recommend}
Breaks: ${phpcomposer:Debian-conflict}
Provides: ${phpcomposer:Debian-provide}
Description: ${phpcomposer:description}
 The Symfony Polyfill project backports features found in the latest PHP
 versions and provides compatibility layers for some extensions and functions.
 It is intended to be used when portability across PHP versions and extensions
 is desired.
 .
 Symfony is a PHP framework, a set of tools and a development methodology.

Package: php-symfony-polyfill-apcu
Multi-Arch: foreign
Architecture: all
Depends: ${misc:Depends}, ${phpcomposer:Debian-require}
Suggests: ${phpcomposer:Debian-suggest}
Recommends: ${phpcomposer:Debian-recommend}
Replaces: ${phpcomposer:Debian-replace}
Breaks: ${phpcomposer:Debian-conflict}, ${phpcomposer:Debian-replace}
Provides: ${phpcomposer:Debian-provide}
Description: ${phpcomposer:description}
 This component provides `apcu_*` functions and the `APCUIterator` class to
 users of the legacy APC extension.
 .
 The Symfony Polyfill project backports features found in the latest PHP
 versions and provides compatibility layers for some extensions and functions.
 It is intended to be used when portability across PHP versions and extensions
 is desired.
 .
 Symfony is a PHP framework, a set of tools and a development methodology.

Package: php-symfony-polyfill-ctype
Multi-Arch: foreign
Architecture: all
Depends: ${misc:Depends}, ${phpcomposer:Debian-require}
Suggests: ${phpcomposer:Debian-suggest}
Recommends: ${phpcomposer:Debian-recommend}
Replaces: ${phpcomposer:Debian-replace}
Breaks: ${phpcomposer:Debian-conflict}, ${phpcomposer:Debian-replace}
Description: ${phpcomposer:description}
 This component provides `ctype_*` functions to users who run php versions
 without the ctype extension.
 .
 The Symfony Polyfill project backports features found in the latest PHP
 versions and provides compatibility layers for some extensions and functions.
 It is intended to be used when portability across PHP versions and extensions
 is desired.
 .
 Symfony is a PHP framework, a set of tools and a development methodology.

Package: php-symfony-polyfill-iconv
Multi-Arch: foreign
Architecture: all
Depends: ${misc:Depends}, ${phpcomposer:Debian-require}
Suggests: ${phpcomposer:Debian-suggest}
Recommends: ${phpcomposer:Debian-recommend}
Replaces: ${phpcomposer:Debian-replace}
Breaks: ${phpcomposer:Debian-conflict}, ${phpcomposer:Debian-replace}
Description: ${phpcomposer:description}
 This component provides a native PHP implementation of the iconv functions
 (short of `ob_iconv_handler`).
 .
 The Symfony Polyfill project backports features found in the latest PHP
 versions and provides compatibility layers for some extensions and functions.
 It is intended to be used when portability across PHP versions and extensions
 is desired.
 .
 Symfony is a PHP framework, a set of tools and a development methodology.

Package: php-symfony-polyfill-intl-grapheme
Multi-Arch: foreign
Architecture: all
Depends: ${misc:Depends}, ${phpcomposer:Debian-require}
Suggests: ${phpcomposer:Debian-suggest}
Recommends: ${phpcomposer:Debian-recommend}
Replaces: ${phpcomposer:Debian-replace}
Breaks: ${phpcomposer:Debian-conflict}, ${phpcomposer:Debian-replace}
Provides: ${phpcomposer:Debian-provide}
Description: ${phpcomposer:description}
 This component provides a partial, native PHP implementation of the Grapheme
 functions from the Intl extension.
 .
  * grapheme_extract: Extract a sequence of grapheme clusters from a
    text buffer, which must be encoded in UTF-8
  * grapheme_stripos: Find position (in grapheme units) of first
    occurrence of a case-insensitive string
  * grapheme_stristr: Returns part of haystack string from the first
    occurrence of case-insensitive needle to the end of haystack
  * grapheme_strlen: Get string length in grapheme units
  * grapheme_strpos: Find position (in grapheme units) of first
    occurrence of a string
  * grapheme_strripos: Find position (in grapheme units) of last
    occurrence of a case-insensitive string
  * grapheme_strrpos: Find position (in grapheme units) of last
    occurrence of a string
  * grapheme_strstr: Returns part of haystack string from the first
    occurrence of needle to the end of haystack
  * grapheme_substr: Return part of a string
 .
 The Symfony Polyfill project backports features found in the latest PHP
 versions and provides compatibility layers for some extensions and functions.
 It is intended to be used when portability across PHP versions and extensions
 is desired.
 .
 Symfony is a PHP framework, a set of tools and a development methodology.

Package: php-symfony-polyfill-intl-icu
Multi-Arch: foreign
Architecture: all
Depends: ${misc:Depends}, ${phpcomposer:Debian-require}
Suggests: ${phpcomposer:Debian-suggest}
Recommends: ${phpcomposer:Debian-recommend}
Replaces: ${phpcomposer:Debian-replace}
Breaks: ${phpcomposer:Debian-conflict}, ${phpcomposer:Debian-replace}
Provides: ${phpcomposer:Debian-provide}
Description: ${phpcomposer:description}
 This component provides fallback implementations when the Intl extension is
 not installed. It is limited to the "en" locale and to:
 .
  * intl_is_failure()
  * intl_get_error_code()
  * intl_get_error_message()
  * intl_error_name()
  * Collator
  * NumberFormatter
  * Locale
  * IntlDateFormatter
 .
 The Symfony Polyfill project backports features found in the latest PHP
 versions and provides compatibility layers for some extensions and functions.
 It is intended to be used when portability across PHP versions and extensions
 is desired.
 .
 Symfony is a PHP framework, a set of tools and a development methodology.

Package: php-symfony-polyfill-intl-idn
Multi-Arch: foreign
Architecture: all
Depends: ${misc:Depends}, ${phpcomposer:Debian-require}
Suggests: ${phpcomposer:Debian-suggest}
Recommends: ${phpcomposer:Debian-recommend}
Replaces: ${phpcomposer:Debian-replace}
Breaks: ${phpcomposer:Debian-conflict}, ${phpcomposer:Debian-replace}
Provides: ${phpcomposer:Debian-provide}
Description: ${phpcomposer:description}
 This component provides a idn_to_ascii and idn_to_utf8 functions to users who
 run PHP versions without the Intl extension.
 .
 The Symfony Polyfill project backports features found in the latest PHP
 versions and provides compatibility layers for some extensions and functions.
 It is intended to be used when portability across PHP versions and extensions
 is desired.
 .
 Symfony is a PHP framework, a set of tools and a development methodology.

Package: php-symfony-polyfill-intl-messageformatter
Multi-Arch: foreign
Architecture: all
Depends: ${misc:Depends}, ${phpcomposer:Debian-require}
Suggests: ${phpcomposer:Debian-suggest}
Recommends: ${phpcomposer:Debian-recommend}
Replaces: ${phpcomposer:Debian-replace}
Breaks: ${phpcomposer:Debian-conflict}, ${phpcomposer:Debian-replace}
Provides: ${phpcomposer:Debian-provide}
Description: ${phpcomposer:description}
 This component provides a fallback implementation for the MessageFormatter
 class provided by the Intl extension.
 .
 The Symfony Polyfill project backports features found in the latest PHP
 versions and provides compatibility layers for some extensions and functions.
 It is intended to be used when portability across PHP versions and extensions
 is desired.
 .
 Symfony is a PHP framework, a set of tools and a development methodology.

Package: php-symfony-polyfill-intl-normalizer
Multi-Arch: foreign
Architecture: all
Depends: ${misc:Depends}, ${phpcomposer:Debian-require}
Suggests: ${phpcomposer:Debian-suggest}
Recommends: ${phpcomposer:Debian-recommend}
Replaces: ${phpcomposer:Debian-replace}
Breaks: ${phpcomposer:Debian-conflict}, ${phpcomposer:Debian-replace}
Provides: ${phpcomposer:Debian-provide}
Description: ${phpcomposer:description}
 This component provides a fallback implementation for the Normalizer class
 provided by the Intl extension.
 .
 The Symfony Polyfill project backports features found in the latest PHP
 versions and provides compatibility layers for some extensions and functions.
 It is intended to be used when portability across PHP versions and extensions
 is desired.
 .
 Symfony is a PHP framework, a set of tools and a development methodology.

Package: php-symfony-polyfill-mbstring
Multi-Arch: foreign
Architecture: all
Depends: ${misc:Depends}, ${phpcomposer:Debian-require}
Suggests: ${phpcomposer:Debian-suggest}
Recommends: ${phpcomposer:Debian-recommend}
Replaces: ${phpcomposer:Debian-replace}
Breaks: ${phpcomposer:Debian-conflict}, ${phpcomposer:Debian-replace}
Description: ${phpcomposer:description}
 This component provides a partial, native PHP implementation for the
 Mbstring extension.
 .
 The Symfony Polyfill project backports features found in the latest PHP
 versions and provides compatibility layers for some extensions and functions.
 It is intended to be used when portability across PHP versions and extensions
 is desired.
 .
 Symfony is a PHP framework, a set of tools and a development methodology.

Package: php-symfony-polyfill-php72
Multi-Arch: foreign
Architecture: all
Depends: ${misc:Depends}, ${phpcomposer:Debian-require}
Suggests: ${phpcomposer:Debian-suggest}
Recommends: ${phpcomposer:Debian-recommend}
Replaces: ${phpcomposer:Debian-replace}
Breaks: ${phpcomposer:Debian-conflict}, ${phpcomposer:Debian-replace}
Provides: ${phpcomposer:Debian-provide}
Description: ${phpcomposer:description}
 This component provides functions unavailable in releases prior to PHP 7.2:
 .
  * is_iterable
  * stream_isatty
  * utf8_encode
  * utf8_decode
  * mb_ord
  * mb_chr
  * mb_scrub
 .
 The Symfony Polyfill project backports features found in the latest PHP
 versions and provides compatibility layers for some extensions and functions.
 It is intended to be used when portability across PHP versions and extensions
 is desired.
 .
 Symfony is a PHP framework, a set of tools and a development methodology.

Package: php-symfony-polyfill-php73
Multi-Arch: foreign
Architecture: all
Depends: ${misc:Depends}, ${phpcomposer:Debian-require}
Suggests: ${phpcomposer:Debian-suggest}
Recommends: ${phpcomposer:Debian-recommend}
Replaces: ${phpcomposer:Debian-replace}
Breaks: ${phpcomposer:Debian-conflict}, ${phpcomposer:Debian-replace}
Provides: ${phpcomposer:Debian-provide}
Description: ${phpcomposer:description}
 This component provides functions unavailable in releases prior to PHP 7.3:
 .
  * array_key_first
  * array_key_last
  * hrtime
  * is_countable
  * JsonException
 .
 The Symfony Polyfill project backports features found in the latest PHP
 versions and provides compatibility layers for some extensions and functions.
 It is intended to be used when portability across PHP versions and extensions
 is desired.
 .
 Symfony is a PHP framework, a set of tools and a development methodology.

Package: php-symfony-polyfill-php74
Multi-Arch: foreign
Architecture: all
Depends: ${misc:Depends}, ${phpcomposer:Debian-require}
Suggests: ${phpcomposer:Debian-suggest}
Recommends: ${phpcomposer:Debian-recommend}
Replaces: ${phpcomposer:Debian-replace}
Breaks: ${phpcomposer:Debian-conflict}, ${phpcomposer:Debian-replace}
Provides: ${phpcomposer:Debian-provide}
Description: ${phpcomposer:description}
 This component provides functions unavailable in releases prior to PHP 7.4:
 .
  * get_mangled_object_vars
  * mb_str_split
  * password_algos
 .
 The Symfony Polyfill project backports features found in the latest PHP
 versions and provides compatibility layers for some extensions and functions.
 It is intended to be used when portability across PHP versions and extensions
 is desired.
 .
 Symfony is a PHP framework, a set of tools and a development methodology.

Package: php-symfony-polyfill-php80
Multi-Arch: foreign
Architecture: all
Depends: ${misc:Depends}, ${phpcomposer:Debian-require}
Suggests: ${phpcomposer:Debian-suggest}
Recommends: ${phpcomposer:Debian-recommend}
Replaces: ${phpcomposer:Debian-replace}
Breaks: ${phpcomposer:Debian-conflict}, ${phpcomposer:Debian-replace}
Provides: ${phpcomposer:Debian-provide}
Description: ${phpcomposer:description}
 This component provides features added to PHP 8.0 core:
 .
  * Stringable interface
  * fdiv
  * ValueError class
  * UnhandledMatchError class
  * FILTER_VALIDATE_BOOL constant
  * get_debug_type
  * PhpToken class
  * preg_last_error_msg
  * str_contains
  * str_starts_with
  * str_ends_with
  * get_resource_id
  * Attribute class
 .
 The Symfony Polyfill project backports features found in the latest PHP
 versions and provides compatibility layers for some extensions and functions.
 It is intended to be used when portability across PHP versions and extensions
 is desired.
 .
 Symfony is a PHP framework, a set of tools and a development methodology.

Package: php-symfony-polyfill-php81
Multi-Arch: foreign
Architecture: all
Depends: ${misc:Depends}, ${phpcomposer:Debian-require}
Suggests: ${phpcomposer:Debian-suggest}
Recommends: ${phpcomposer:Debian-recommend}
Replaces: ${phpcomposer:Debian-replace}
Breaks: ${phpcomposer:Debian-conflict}, ${phpcomposer:Debian-replace}
Provides: ${phpcomposer:Debian-provide}
Description: ${phpcomposer:description}
 This component provides features added to PHP 8.1 core:
 .
  * array_is_list
  * MYSQLI_REFRESH_REPLICA constant
  * ReturnTypeWillChange attribute
  * CURLStringFile

Package: php-symfony-polyfill-php82
Multi-Arch: foreign
Architecture: all
Depends: ${misc:Depends}, ${phpcomposer:Debian-require}
Suggests: ${phpcomposer:Debian-suggest}
Recommends: ${phpcomposer:Debian-recommend}
Replaces: ${phpcomposer:Debian-replace}
Breaks: ${phpcomposer:Debian-conflict}, ${phpcomposer:Debian-replace}
Provides: ${phpcomposer:Debian-provide}
Description: ${phpcomposer:description}
 This component provides features added to PHP 8.2 core:
 .
  * AllowDynamicProperties
  * SensitiveParameter
  * SensitiveParameterValue
  * Random\Engine
  * Random\Engine\CryptoSafeEngine
  * Random\Engine\Secure
  * odbc_connection_string_is_quoted()
  * odbc_connection_string_should_quote()
  * odbc_connection_string_quote()
  * ini_parse_quantity()

Package: php-symfony-polyfill-php83
Multi-Arch: foreign
Architecture: all
Depends: ${misc:Depends}, ${phpcomposer:Debian-require}
Suggests: ${phpcomposer:Debian-suggest}
Recommends: ${phpcomposer:Debian-recommend}
Replaces: ${phpcomposer:Debian-replace}
Breaks: ${phpcomposer:Debian-conflict}, ${phpcomposer:Debian-replace}
Provides: ${phpcomposer:Debian-provide}
Description: ${phpcomposer:description}
 This component provides features added to PHP 8.3 core:
 .
  * json_validate
  * Override
  * mb_str_pad
  * ldap_exop_sync
  * ldap_connect_wallet
  * stream_context_set_options
  * str_increment and str_decrement
  * Date*Exception/Error classes
  * SQLite3Exception

Package: php-symfony-polyfill-php84
Multi-Arch: foreign
Architecture: all
Depends: ${misc:Depends}, ${phpcomposer:Debian-require}
Suggests: ${phpcomposer:Debian-suggest}
Recommends: ${phpcomposer:Debian-recommend}
Replaces: ${phpcomposer:Debian-replace}
Breaks: ${phpcomposer:Debian-conflict}, ${phpcomposer:Debian-replace}
Provides: ${phpcomposer:Debian-provide}
Description: ${phpcomposer:description}
 This component provides features added to PHP 8.4 core.

Package: php-symfony-polyfill-util
Multi-Arch: foreign
Architecture: all
Depends: ${misc:Depends}, ${phpcomposer:Debian-require}
Suggests: ${phpcomposer:Debian-suggest}
Recommends: ${phpcomposer:Debian-recommend}
Replaces: ${phpcomposer:Debian-replace}
Breaks: ${phpcomposer:Debian-conflict}, ${phpcomposer:Debian-replace}
Provides: ${phpcomposer:Debian-provide}
Description: ${phpcomposer:description}
 This component provides binary-safe string functions, using the mbstring
 extension when available.
 .
 The Symfony Polyfill project backports features found in the latest PHP
 versions and provides compatibility layers for some extensions and functions.
 It is intended to be used when portability across PHP versions and extensions
 is desired.
 .
 Symfony is a PHP framework, a set of tools and a development methodology.

Package: php-symfony-polyfill-uuid
Multi-Arch: foreign
Architecture: all
Depends: ${misc:Depends}, ${phpcomposer:Debian-require}
Suggests: ${phpcomposer:Debian-suggest}
Recommends: ${phpcomposer:Debian-recommend}
Replaces: ${phpcomposer:Debian-replace}
Breaks: ${phpcomposer:Debian-conflict}, ${phpcomposer:Debian-replace}
Description: ${phpcomposer:description}
 This component provides `uuid_*` functions to users who run PHP versions
 without the uuid extension.
 .
 The Symfony Polyfill project backports features found in the latest PHP
 versions and provides compatibility layers for some extensions and functions.
 It is intended to be used when portability across PHP versions and extensions
 is desired.
 .
 Symfony is a PHP framework, a set of tools and a development methodology.

Package: php-symfony-polyfill-xml
Multi-Arch: foreign
Architecture: all
Depends: ${misc:Depends}, ${phpcomposer:Debian-require}
Suggests: ${phpcomposer:Debian-suggest}
Recommends: ${phpcomposer:Debian-recommend}
Replaces: ${phpcomposer:Debian-replace}
Breaks: ${phpcomposer:Debian-conflict}, ${phpcomposer:Debian-replace}
Provides: ${phpcomposer:Debian-provide}
Description: ${phpcomposer:description}
 Deprecated polyfill, for backward compatibility only.
 .
 The Symfony Polyfill project backports features found in the latest PHP
 versions and provides compatibility layers for some extensions and functions.
 It is intended to be used when portability across PHP versions and extensions
 is desired.
 .
 Symfony is a PHP framework, a set of tools and a development methodology.
